package com.capicp.dsyutils

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import cl.dsarhoya.dsyutils.view.forms.FormHandler
import cl.dsarhoya.dsyutils.view.forms.annotations.ModelInputView
import com.capicp.dsyutils.models.User

class FormActivity : AppCompatActivity() {



    companion object formView {
        @ModelInputView(propertyName = "name")
        lateinit var nameET:EditText

        @ModelInputView(setMethod = "setBetterEmail")
        lateinit var emaildET:EditText
    }

    val userModel = User()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        nameET = findViewById(R.id.name)
        emaildET = findViewById(R.id.email)
    }

    fun submitForm(v: View){

        var form = FormHandler(formView, userModel)
        form.submit()

        Toast.makeText(this, "Envio exitoso", Toast.LENGTH_SHORT).show()
    }
}
