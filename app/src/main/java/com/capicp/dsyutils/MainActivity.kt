package com.capicp.dsyutils

import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import cl.dsarhoya.dsyutils.SecurityUtils
import android.content.pm.PackageManager.GET_SIGNATURES
import android.view.View


class MainActivity : AppCompatActivity() {

    var signature = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //val pi = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
        //Log.d(javaClass.simpleName, pi.signatures[0].toCharsString())

        if (SecurityUtils.verifyTamperRisk(this, signature)){
            Log.d(javaClass.simpleName, "Vulnerable.")
        }

    }

    fun openForm(v: View){
        var i = Intent(this, FormActivity::class.java)
        startActivity(i)
    }
}
