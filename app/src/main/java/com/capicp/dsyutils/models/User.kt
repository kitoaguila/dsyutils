package com.capicp.dsyutils.models

/**
 * Created by cpine on 12/2/2018.
 */
class User(var name:String, var email:String, var rut:String, var dateOfBirth:String) {

    constructor() : this("", "", "", ""){

    }

    fun setBetterEmail(email:String){
        this.email = "thebest_$email"
    }

}