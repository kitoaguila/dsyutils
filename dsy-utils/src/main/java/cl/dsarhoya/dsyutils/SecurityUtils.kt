package cl.dsarhoya.dsyutils

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.ApplicationInfo.FLAG_DEBUGGABLE
import android.content.pm.PackageManager
import android.content.pm.PackageManager.GET_SIGNATURES
import android.content.pm.Signature
import android.util.Log


/**
 * Created by cpine on 29/1/2018.
 */
class SecurityUtils {

    companion object {


        fun verifyTamperRisk(context: Context, key:String):Boolean{
            return !checkGooglePlayStoreInstaller(context) || !isSignedWith(context, key)
                || isDebuggable(context)
        }

        fun checkGooglePlayStoreInstaller(context: Context): Boolean {
            val installerPackageName = context.getPackageManager()
                    .getInstallerPackageName(context.getPackageName())

            if (installerPackageName != null && installerPackageName!!.startsWith("com.google.android")){
                Log.d(javaClass.simpleName, "Installed with Play Store")
                return true
            }else{
                Log.d(javaClass.simpleName, "No installed with Play Store")
                return false

            }
        }

        fun isDebuggable(context: Context): Boolean {
            if (context.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE !== 0){
                Log.d(javaClass.simpleName, "Debuggable")
                return true
            }else{
                Log.d(javaClass.simpleName, "No Debuggable")
                return false
            }

        }

        fun isSignedWith(context: Context, key:String): Boolean {

            var releaseSign = Signature(key)
            try {
                val pm = context.packageManager
                val pi = pm.getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES)
                for (sig in pi.signatures) {
                    if (sig.equals(releaseSign)) {
                        Log.d(javaClass.simpleName, "Signed")
                        return true
                    }
                }
            } catch (e: Exception) {
                Log.w(javaClass.simpleName, e)

                // Return true if we can't figure it out, just to play it safe
                return true
            }
            Log.d(javaClass.simpleName, "No signed")
            return false
        }

        fun isEmulator(): Boolean {
            try {
                val systemPropertyClazz = Class
                        .forName("android.os.SystemProperties")
                val kernelQemu = getProperty(systemPropertyClazz,
                        "ro.kernel.qemu").length > 0
                val hardwareGoldfish = getProperty(systemPropertyClazz,
                        "ro.hardware") == "goldfish"
                val modelSdk = getProperty(systemPropertyClazz,
                        "ro.product.model") == "sdk"
                if (kernelQemu || hardwareGoldfish || modelSdk) {
                    Log.d(javaClass.simpleName, "Running on emulator")

                    return true
                }
            } catch (e: Exception) {
                // error assumes emulator
            }
            Log.d(javaClass.simpleName, "Running on device")
            return false
        }

        @Throws(Exception::class)
        private fun getProperty(clazz: Class<*>, propertyName: String): String {
            return clazz.getMethod("get", *arrayOf<Class<*>>(String::class.java))
                    .invoke(clazz, *arrayOf<Any>(propertyName)) as String
        }
    }
}