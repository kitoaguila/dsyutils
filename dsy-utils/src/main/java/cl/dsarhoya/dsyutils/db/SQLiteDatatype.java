package cl.dsarhoya.dsyutils.db;

/**
 * Created by capi on 21/04/17.
 */

public enum SQLiteDatatype {
    TEXT("TEXT"), INTEGER("INTEGER"), REAL("REAL"), BOOLEAN("INTEGER");

    private String value;

    SQLiteDatatype(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
