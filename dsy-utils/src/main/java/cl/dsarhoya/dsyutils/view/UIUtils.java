package cl.dsarhoya.dsyutils.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;

/**
 * Created by capi on 8/17/17.
 */

public class UIUtils {

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static void exchangeVisibility(final View viewToShow, final View viewToHide) {


        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int animTime = 500;

            viewToHide.setVisibility(View.GONE);
            viewToHide.animate().setDuration(animTime).alpha(0)
                    .setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    viewToHide.setVisibility(View.GONE);
                }
            });

            viewToShow.setVisibility(View.VISIBLE);
            viewToShow.animate().setDuration(animTime).alpha(1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    viewToShow.setVisibility(View.VISIBLE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            viewToShow.setVisibility(View.VISIBLE);
            viewToHide.setVisibility(View.GONE);
        }
    }
}
