package cl.dsarhoya.dsyutils.view.custom

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import android.widget.EditText
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * Created by cpine on 27/11/2017.
 */
class RUTEditText(context: Context?, attrs: AttributeSet?) : EditText(context, attrs) {

    init {

        inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD

        val textWatcher:TextWatcher = object: TextWatcher{

            var lastTextState = ""
            var inputClosed = false
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                lastTextState = s.toString()

                inputClosed = if (lastTextState.length > 0 && lastTextState[lastTextState.length-1].equals('k') && after > 0) true else false

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (!inputClosed && s.toString().matches(Regex("[-'.'k0-9]+$"))){
                    var rawText:String = s.toString().replace(".", "").replace("-", "")

                    var lastIndex = rawText.length - 1

                    if (lastIndex > -1 ){
                        var rutNumber =  if (rawText.length > 1)  rawText.subSequence(0, lastIndex ).toString() else ""

                        var validatorDigit = rawText[lastIndex]

                        try{
                            rutNumber = String.format("%,d",  rutNumber.toLong()).replace(",", ".")
                        }catch (ex:NumberFormatException){
                            Log.e(javaClass.simpleName, ex.message, ex)
                        }

                        this@RUTEditText.removeTextChangedListener(this)
                        this@RUTEditText.setText(String.format("%s-%s", rutNumber, validatorDigit))

                        this@RUTEditText.addTextChangedListener(this)
                    }else{
                        this@RUTEditText.removeTextChangedListener(this)
                        this@RUTEditText.setText("")
                        this@RUTEditText.addTextChangedListener(this)
                    }
                }else{
                    this@RUTEditText.removeTextChangedListener(this)
                    this@RUTEditText.setText(lastTextState)
                    this@RUTEditText.addTextChangedListener(this)
                }

                this@RUTEditText.setSelection(this@RUTEditText.text.length)


            }

        }

        addTextChangedListener(textWatcher);
    }
}