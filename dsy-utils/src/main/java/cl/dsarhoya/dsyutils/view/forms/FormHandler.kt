package cl.dsarhoya.dsyutils.view.forms

import android.util.Log
import android.widget.EditText
import cl.dsarhoya.dsyutils.view.forms.annotations.ModelInputView
import kotlin.reflect.KFunction
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty1
import kotlin.reflect.full.*

/**
 * Created by cpine on 12/2/2018.
 */
class FormHandler  {

    var viewsContainer:Any
    var model:Any

    var modelProperties:Collection<KProperty1<*,*>>

    var viewFields:Collection<KProperty1<*,*>>
    var modelMethods:Collection<KFunction<*>>

    constructor(viewsContainer: Any, model:Any ){
        this.viewsContainer = viewsContainer
        this.model = model
        modelProperties = model::class.memberProperties
        modelMethods = model::class.declaredMemberFunctions
        viewFields = viewsContainer::class.memberProperties
    }

    fun submit():Any{

        for (field in viewFields){
            var ann = field.findAnnotation<ModelInputView>()
            if (ann != null){
                var inputView = field.getter.call(viewsContainer) as EditText

                var value = inputView.text.toString()

                if (!ann.propertyName.isEmpty()){
                    setPropertyByName(ann.propertyName, value)
                }else{
                    if (!ann.setMethod.isEmpty()){
                        setPropertyByMethod(ann.setMethod, value)
                    }
                }

                //model.javaClass.getMethod("set${ann.propertyName.capitalize()}", String.javaClass).invoke(model, inputView.text )
            }
        }

        return model
    }

    private fun setPropertyByMethod(methodName:String, value:String){
        var method = modelMethods.find { it.name == methodName } as KFunction<*>
        method.call(model, value)
    }


    private fun setPropertyByName(propertyName:String, value:String){
        val modelProperty = modelProperties.find { it.name == propertyName } as KMutableProperty<*>

        if (modelProperty != null){
            modelProperty.setter.call(model, value)
        }
    }

}