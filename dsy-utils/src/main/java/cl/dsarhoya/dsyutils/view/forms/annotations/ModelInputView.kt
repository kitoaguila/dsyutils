package cl.dsarhoya.dsyutils.view.forms.annotations

/**
 * Created by cpine on 13/2/2018.
 */

annotation class ModelInputView(val propertyName:String = "", val setMethod:String = "")